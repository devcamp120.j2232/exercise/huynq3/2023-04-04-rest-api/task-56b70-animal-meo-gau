package com.devcamp.animalapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.animalapi.models.Cat;
import com.devcamp.animalapi.models.Dog;

@CrossOrigin
@RequestMapping
@RestController
public class restapicontroller {
    @GetMapping("/cats")
    public ArrayList getAllCats() {
        Cat cat1 = new Cat("Mimi");
        Cat cat2 = new Cat("Lili");
        Cat cat3 = new Cat("Nana");
        ArrayList<Cat> listcats = new ArrayList<>();
        listcats.add(cat1);
        listcats.add(cat2);
        listcats.add(cat3);

        return listcats;

    }
    @GetMapping("/dogs")
    public ArrayList getAllDogs() {
        Dog dog1 = new Dog("Lulu");
        Dog dog2 = new Dog("Toto");
        Dog dog3 = new Dog("Bibi");
        ArrayList<Dog> listdogs = new ArrayList<>();
        listdogs.add(dog1);
        listdogs.add(dog2);
        listdogs.add(dog3);

        return listdogs;

    }
}
