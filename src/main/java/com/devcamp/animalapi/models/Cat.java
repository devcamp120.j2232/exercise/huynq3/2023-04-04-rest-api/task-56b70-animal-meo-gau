package com.devcamp.animalapi.models;

public class Cat {
    private String name;

    public Cat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Cat [name=" + name + "]";
    }
    public void greets(){
        System.out.println("Meow");
    }
    public void greets(Dog target){
        System.out.println("Meoooooowwwwwwww");
    }

    
    
}
