package com.devcamp.animalapi.models;

public class Dog {
    private String name;

    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void greets(){
        System.out.println("Woof");
    }
    public void greets(Dog target){
        System.out.println("Woooooooofffff");
    }
}
